import os

import mysql.connector


def get_connection():
    connection = mysql.connector.connect(
        host=os.getenv("MYSQL_HOST", "localhost"),
        port=os.getenv("MYSQL_PORT", 3306),
        user=os.getenv("MYSQL_USER", "2facesnet_user"),
        password=os.getenv("MYSQL_PASSWORD", "2facesnet_password"),
        database=os.getenv("MYSQL_DATABASE", "2facesnet_db")
    )
    return connection
